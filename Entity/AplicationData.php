<?php

namespace JoberBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AplicationData
 *
 * @ORM\Table(name="aplication_data")
 * @ORM\Entity(repositoryClass="JoberBundle\Repository\AplicationDataRepository")
 */
class AplicationData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="imie", type="string", length=255)
     * @Assert\NotBlank(message="pole imie nie moze być puste")
     * @Assert\Regex(
     *     pattern = "/^[a-zA-Z]/",
     
     * )
     * @Assert\Length(min="4",max = 10, 
     *                minMessage="Imie musi posiadać conajmniej cztery litery",
     *                maxMessage = "imie moze mieć tylko 10 liter"
     * 
     * )
     */
    private $imie;

    /**
     * @var string
     *
     * @ORM\Column(name="naziwsko", type="string", length=255)
     * @Assert\NotBlank(message="pole nazwisko nie moze być puste")
     * @Assert\Regex(
     *     pattern = "/^[a-ząćęłńóśźżA-ZĄĆĆŁŃÓŚŹŻ]/",
     *     
     * )
     * @Assert\Length(min="4", max = 10, 
     *                minMessage="Nazwisko musi posiadać conajmniej trzy litery",
     *                maxMessage = "nazwisko możoe mieć tylko 10 liter"
     * )
     */
    
    private $nazwisko;

    /**
     * @var integer
     *
     * @ORM\Column(name="doswiadczenie", type="integer", length=255)
     */
    private $doswiadczenie;

    /**
     * @var string
     *
     * @ORM\Column(name="miasto", type="string", length=255)
     * @Assert\NotBlank(message="pole miasto nie moze być puste")
     * @Assert\Regex(
     *     pattern = "/^[a-zA-Z]/",
     *     
     * )
     * @Assert\Length(min="4",max = 10, 
     *                minMessage="Miasto musi posiadać conajmniej cztery litery",
     *                maxMessage = "miasto możoe mieć tylko 10 liter"
     * 
     * )
     */
    private $miasto;

    /**
     * @var string
     *
     * @ORM\Column(name="kraj", type="string", length=255)
     * 
     */
    private $kraj;

    /**
     * @var string
     *
     * @ORM\Column(name="dyspozycyjnosc", type="json_array", length=255)
     * @Assert\NotBlank(message="pole miasto nie moze być puste")
     */
    private $dyspozycyjnosc;

    /**
     * @var string
     *
     * @ORM\Column(name="profesja", type="string", length=255)
     */
    private $profesja;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imie
     *
     * @param string $imie
     *
     * @return AplicationData
     */
    public function setImie($imie)
    {
        $this->imie = $imie;

        return $this;
    }

    /**
     * Get imie
     *
     * @return string
     */
    public function getImie()
    {
        return $this->imie;
    }

    /**
     * Set nazwisko
     *
     * @param string $nazwisko
     *
     * @return AplicationData
     */
    public function setNazwisko($nazwisko)
    {
        $this->nazwisko = $nazwisko;

        return $this;
    }

    /**
     * Get nazwisko
     *
     * @return string
     */
    public function getNazwisko()
    {
        return $this->nazwisko;
    }

    /**
     * Set doswiadczenie
     *
     * @param integer $doswiadczenie
     *
     * @return AplicationData
     */
    public function setDoswiadczenie($doswiadczenie)
    {
        $this->doswiadczenie = $doswiadczenie;

        return $this;
    }

    /**
     * Get doswiadczenie
     *
     * @return string
     */
    public function getDoswiadczenie()
    {
        return $this->doswiadczenie;
    }

    /**
     * Set miasto
     *
     * @param string $miasto
     *
     * @return AplicationData
     */
    public function setMiasto($miasto)
    {
        $this->miasto = $miasto;

        return $this;
    }

    /**
     * Get miasto
     *
     * @return string
     */
    public function getMiasto()
    {
        return $this->miasto;
    }

    /**
     * Set kraj
     *
     * @param string $kraj
     *
     * @return AplicationData
     */
    public function setKraj($kraj)
    {
        $this->kraj = $kraj;

        return $this;
    }

    /**
     * Get kraj
     *
     * @return string
     */
    public function getKraj()
    {
        return $this->kraj;
    }

    /**
     * Set dyspozycyjnosc
     *
     * @param json_array $dyspozycyjnosc
     *
     * @return AplicationData
     */
    public function setDyspozycyjnosc($dyspozycyjnosc)
    {
        $this->dyspozycyjnosc = $dyspozycyjnosc;

        return $this;
    }

    /**
     * Get dyspozycyjnosc
     *
     * @return array
     */
    public function getDyspozycyjnosc()
    {
        return array($this->dyspozycyjnosc);
    }

    /**
     * Set profesja
     *
     * @param string $profesja
     *
     * @return AplicationData
     */
    public function setProfesja($profesja)
    {
        $this->profesja = $profesja;

        return $this;
    }

    /**
     * Get profesja
     *
     * @return string
     */
    public function getProfesja()
    {
        return $this->profesja;
    }
    
}

