<?php


namespace JoberBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class AplicationDataType extends AbstractType
{
    
    const ZDALNA = 'praca zdalna';
    const MIEJSCU = 'praca na miejscu';
 
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder 
			->add('imie')
            ->add('nazwisko')
            ->add('doswiadczenie')
            ->add('miasto')
            ->add('kraj',ChoiceType::class, 
                    array('choices'  => array(
                            'Polska' => 'Polska',
                            'Niemcy' =>  'Niemcy',
                            'Francja' => 'Francja',
                            'Wielka Brytanua'=>'Wielka Brytania'
                 )))
            ->add('dyspozycyjnosc',ChoiceType::class,
                    array('choices' => array(
                            'praca zdalna' => self::ZDALNA,
                            'praca na miejscu' => self::MIEJSCU),
                            'choices_as_values' => true,
                            'multiple'=>true,
                            'required' => false,
                            'expanded'=>true))
            ->add('profesja',ChoiceType::class, 
                    array('choices'  => array(
                            'Programista' => 'Programista',
                            'Koder' =>  'Koder',
                            'Designer' => 'Designer',
                 )))
            ->add('save', SubmitType::class)
			
		
        ;
    }
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'JoberBundle\Entity\AplicationData',
			'csrf_protection' => false,
		));
	}
    
   
	
	
}
