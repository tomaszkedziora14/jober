<?php

namespace JoberBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JoberBundle\Entity\AplicationData;
use Symfony\Component\HttpFoundation\Request;
use JoberBundle\Form\AplicationDataType;


class JoberController extends Controller
{  
    public function createAction(Request $request)
    {
            $data = new AplicationData();
            $data->setDyspozycyjnosc(AplicationDataType::MIEJSCU, AplicationDataType::ZDALNA);
            
                $form = $this->createForm(AplicationDataType::class, $data,array(
                'action' => $this->generateUrl('jober_formaplicationdata'),
                'method' => 'POST'));
             
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $data = $form->getData();
                            $em = $this->getDoctrine()->getManager();
                            $em->persist($data);
                            $em->flush();
                            $id = $data->getId(); 
                            $newid['id'] = $id;
                            return $this->redirect($this->generateUrl('jober_showdata',$newid));
                    }

                    return $this->render('JoberBundle:Jober:joberform.html.twig',['form'=>$form->createView()]);

    }
    
    public function showAplicationDataAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('JoberBundle:AplicationData')->find($id);
        return $this->render('JoberBundle:Jober:showdata.html.twig',['data'=>$data]);
    }
}
